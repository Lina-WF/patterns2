﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Patterns
{
    class Project
    {
        private int projectLevel;

        public Project(int projectLevel)
        {
            this.projectLevel = projectLevel;
        }

        public int GetProjectLevel()
        {
            return projectLevel;
        }
    }

    interface IEmploee
    {
        string GetName();
        int GetProjectsNum();
        int GetLevel();
        void AddProject(Project project);

    }

    class Junior : IEmploee
    {
        private string name;
        private int level;
        private List<Project> projects;

        public Junior(string name, int level)
        {
            this.name = name;
            this.level = level;
            projects = new List<Project>();
        }

        public string GetName()
        {
            return name;
        }

        public int GetProjectsNum()
        {
            return projects.Count();
        }

        public int GetLevel()
        {
            return level;
        }

        public void AddProject(Project project)
        {
            projects.Add(project);
        }
    }

    class Middle : IEmploee
    {
        private string name;
        private int level;
        private List<Project> projects;

        public Middle(string name, int level)
        {
            this.name = name;
            this.level = level;
            projects = new List<Project>();
        }

        public string GetName()
        {
            return name;
        }

        public int GetProjectsNum()
        {
            return projects.Count();
        }

        public int GetLevel()
        {
            return level;
        }

        public void AddProject(Project project)
        {
            projects.Add(project);
        }
    }

    class Senior : IEmploee
    {
        private string name;
        private int level;
        private List<Project> projects;

        public Senior(string name, int level)
        {
            this.name = name;
            this.level = level;
            projects = new List<Project>();
        }

        public string GetName()
        {
            return name;
        }

        public int GetProjectsNum()
        {
            return projects.Count();
        }

        public int GetLevel()
        {
            return level;
        }

        public void AddProject(Project project)
        {
            projects.Add(project);
        }
    }

    class Team : IEmploee
    {
        private string name;
        private int level = 0;
        private List<IEmploee> emploees;
        private List<Project> projects;

        public Team(string name)
        {
            this.name = name;
            emploees = new List<IEmploee>();
            projects = new List<Project>();
        }

        public string GetName()
        {
            string names = "Team " + name + ":\n";
            foreach (IEmploee emploee in emploees)
            {
                names += "\t" + emploee.GetName() + "\n";
            }
            return names;
        }

        public int GetProjectsNum()
        {
            return projects.Count();
        }

        public int GetLevel()
        {
            return level;
        }

        public void AddProject(Project project)
        {
            projects.Add(project);
            foreach(IEmploee emploee in emploees)
            {
                emploee.AddProject(project);
            }
        }

        public void AddEmploee(IEmploee emploee)
        {
            emploees.Add(emploee);
            level = (level + emploee.GetLevel()) / emploees.Count();
        }
    }


    abstract class EmploeeLink
    {
        public IEmploee current;
        public EmploeeLink next;
        public abstract bool Request(Project project);
    }

    class Handling : EmploeeLink
    {
        public Handling(IEmploee current, EmploeeLink next)
        {
            this.current = current;
            this.next = next;
        }

        public override bool Request(Project project)
        {
            if (project.GetProjectLevel() <= current.GetLevel() && current.GetProjectsNum() < 3)
            {
                current.AddProject(project);
                return true;
            }
            if (next != null)
            {
                return next.Request(project);
            }
            return false;
        }
    }

    class Programm
    {
        static void Main(string[] args)
        {
            List<IEmploee> emploees = new List<IEmploee>();
            emploees.Add(new Junior("Jake", 2));
            emploees.Add(new Middle("Antonie", 5));
            emploees.Add(new Senior("Ben", 10));
            Team team1 = new Team("1");
            team1.AddEmploee(emploees[1]);
            team1.AddEmploee(emploees[2]);
            emploees.Insert(0, team1);

            Handling handlingHead = new Handling(emploees[0], null);
            Handling handling1 = handlingHead;

            for (int i = 1; i < emploees.Count; i++)
            {
                Handling handling2 = new Handling(emploees[i], null);
                handling1.next = handling2;
                handling1 = handling2;
            }

            Project project1 = new Project(1);
            if (handlingHead.Request(project1)) Console.WriteLine("Success");
            else Console.WriteLine("Failed");

            Project project2 = new Project(3);
            if (handlingHead.Request(project2)) Console.WriteLine("Success");
            else Console.WriteLine("Failed");

            Project project3 = new Project(8);
            if (handlingHead.Request(project3)) Console.WriteLine("Success");
            else Console.WriteLine("Failed");

            Project project4 = new Project(6);
            if (handlingHead.Request(project4)) Console.WriteLine("Success");
            else Console.WriteLine("Failed");

            Project project5 = new Project(2);
            if (handlingHead.Request(project5)) Console.WriteLine("Success");
            else Console.WriteLine("Failed");

            Project project6 = new Project(3);
            if (handlingHead.Request(project6)) Console.WriteLine("Success");
            else Console.WriteLine("Failed");

            Console.WriteLine();

            foreach (IEmploee emploee in emploees)
            {
                Console.WriteLine("Who:" + emploee.GetName() + "\nNumber of projects: " + emploee.GetProjectsNum() + "\n");
            }

            Console.ReadLine();
        }
    }
}
